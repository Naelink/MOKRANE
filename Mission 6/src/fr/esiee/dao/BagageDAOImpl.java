package fr.esiee.dao;

import fr.esiee.dao.BagageDAO;
import fr.esiee.model.Bagage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BagageDAOImpl implements BagageDAO {
    private final String url = "jdbc:mysql://localhost:3306/easyline6";
    private final String user = "nael";
    private final String password = "toor";

    @Override
    public void addBagage(Bagage bagage) {
        String query = "INSERT INTO bagage (numero, couleur, poids) VALUES (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, bagage.getNumero());
            preparedStatement.setString(2, bagage.getCouleur());
            preparedStatement.setDouble(3, bagage.getPoids());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateBagage(Bagage bagage) {
        String query = "UPDATE bagage SET couleur = ?, poids = ? WHERE numero = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, bagage.getCouleur());
            preparedStatement.setDouble(2, bagage.getPoids());
            preparedStatement.setString(3, bagage.getNumero());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBagage(String numero) {
        String query = "DELETE FROM bagage WHERE numero = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, numero);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Bagage getBagageByNumero(String numero) {
        String query = "SELECT * FROM bagage WHERE numero = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, numero);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    String couleur = resultSet.getString("couleur");
                    double poids = resultSet.getDouble("poids");
                    return new Bagage(numero, couleur, poids);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Bagage> getAllBagages() {
        List<Bagage> bagages = new ArrayList<>();
        String query = "SELECT * FROM bagage";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            while (resultSet.next()) {
                String numero = resultSet.getString("numero");
                String couleur = resultSet.getString("couleur");
                double poids = resultSet.getDouble("poids");
                bagages.add(new Bagage(numero, couleur, poids));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bagages;
    }
    public List<Bagage> getBagagesByVoyageurId(int voyageurId) {
        List<Bagage> bagages = new ArrayList<>();
        String query = "SELECT * FROM bagage WHERE voyageur_id = ?"; // Assurez-vous que le nom de colonne est correct

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setInt(1, voyageurId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String numero = resultSet.getString("numero");
                    String couleur = resultSet.getString("couleur");
                    double poids = resultSet.getDouble("poids");
                    Bagage bagage = new Bagage(numero, couleur, poids);
                    bagages.add(bagage);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bagages;
    }
}
