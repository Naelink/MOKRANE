package fr.esiee.dao;

import fr.esiee.model.Voyageur;

import java.util.List;

public interface VoyageurDAO {
    void addVoyageur(Voyageur voyageur);
    Voyageur getVoyageurById(int id);
    List<Voyageur> getAllVoyageurs();
    void updateVoyageur(Voyageur voyageur);
    void deleteVoyageur(int id);
}
