package fr.esiee.dao;

import fr.esiee.model.Bagage;

import java.util.List;

public interface BagageDAO {
    // Ajouter un bagage dans la base de données
    void addBagage(Bagage bagage);

    // Mettre à jour les informations d'un bagage
    void updateBagage(Bagage bagage);

    // Supprimer un bagage de la base de données
    void deleteBagage(String numero);

    // Récupérer un bagage par son numéro
    Bagage getBagageByNumero(String numero);

    // Récupérer tous les bagages
    List<Bagage> getAllBagages();
}
