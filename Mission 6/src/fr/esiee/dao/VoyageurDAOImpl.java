package fr.esiee.dao;

import fr.esiee.model.Voyageur;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VoyageurDAOImpl implements VoyageurDAO {
    private final String url = "jdbc:mysql://localhost:3306/easyline6";
    private final String user = "nael";
    private final String password = "toor";

    @Override
    public void addVoyageur(Voyageur voyageur) {
        // Implémentation de l'ajout d'un voyageur
        // Exemple: String query = "INSERT INTO voyageur (...) VALUES (?, ?, ...)";
        // Utilisez try-with-resources pour établir la connexion et exécuter la requête
    }

    @Override
    public Voyageur getVoyageurById(int id) {
        // Implémentation de la récupération d'un voyageur par son ID
        return null;
    }

    @Override
    public List<Voyageur> getAllVoyageurs() {
        // Implémentation de la récupération de tous les voyageurs
        return new ArrayList<>();
    }

    @Override
    public void updateVoyageur(Voyageur voyageur) {
        // Implémentation de la mise à jour d'un voyageur
    }

    @Override
    public void deleteVoyageur(int id) {
        // Implémentation de la suppression d'un voyageur
    }
}
