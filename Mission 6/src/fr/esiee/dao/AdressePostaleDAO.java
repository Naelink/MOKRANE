package fr.esiee.dao;

import fr.esiee.model.AdressePostale;

import java.util.List;

public interface AdressePostaleDAO {
    void addAdressePostale(AdressePostale adressePostale);
    AdressePostale getAdressePostaleById(int id);
    List<AdressePostale> getAllAdressesPostales();
    void updateAdressePostale(AdressePostale adressePostale);
    void deleteAdressePostale(int id);
}
