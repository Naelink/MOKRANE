package fr.esiee;

import fr.esiee.dao.*;
import fr.esiee.model.AdressePostale;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;

import java.sql.Connection;

public class Main {
    public static void main(String[] args) {
        BagageDAO bagageDAO = new BagageDAOImpl();
        Bagage nouveauBagage = new Bagage("123456", "Rouge", 15.0);
        bagageDAO.addBagage(nouveauBagage);

        System.out.println("Bagage inséré avec succès !");

    }

}

