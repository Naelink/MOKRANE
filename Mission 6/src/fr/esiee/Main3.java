package fr.esiee;

import fr.esiee.dao.BagageDAOImpl;
import fr.esiee.dao.VoyageurDAOImpl;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;

import java.util.List;

public class Main3 {
    public static void main(String[] args) {
        int voyageurId = 2; // ID du voyageur à afficher

        VoyageurDAOImpl voyageurDAO = new VoyageurDAOImpl();
        BagageDAOImpl bagageDAO = new BagageDAOImpl();

        // Récupérer le voyageur par son ID
        Voyageur voyageur = voyageurDAO.getVoyageurById(voyageurId);

        if (voyageur != null) {
            // Afficher les informations du voyageur
            voyageur.afficherInfo();

            // Récupérer et afficher les bagages du voyageur
            List<Bagage> bagages = bagageDAO.getBagagesByVoyageurId(voyageurId);
            for (Bagage bagage : bagages) {
                bagage.afficher();
            }
        } else {
            System.out.println("Voyageur avec l'ID " + voyageurId + " introuvable.");
        }
    }
}
