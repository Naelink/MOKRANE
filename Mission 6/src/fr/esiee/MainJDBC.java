package fr.esiee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainJDBC {

    public static void main(String[] args){
        String url = "jdbc:mysql://localhost:3306/easyline6";
        String user = "nael";
        String pwd = "toor";
        try {
            Connection conn=DriverManager.getConnection(url,user,pwd);
            System.out.println("Connexion OK");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.append("driver ok");
        }catch(Exception e){
            e.printStackTrace();
        }

        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connexion = DriverManager.getConnection(url, user, pwd);
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT * FROM bagage;");
            System.out.println("ID\tPOIDS\t\t");
            System.out.println("------------------------------------------------");
            while (resultat.next()) {
                int id = resultat.getInt("id");
                String poids = resultat.getString("poids");

                System.out.println(id + "\t" + poids + "\t\t" );
            }
        } catch (ClassNotFoundException e) {
            System.err.println("Le driver n'a pas été trouvé.");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données.");
            e.printStackTrace();
        } finally {
            if (resultat != null) {
                try {
                    resultat.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
