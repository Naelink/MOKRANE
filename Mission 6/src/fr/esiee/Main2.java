package fr.esiee;

import fr.esiee.dao.AdressePostaleDAOImpl;
import fr.esiee.dao.BagageDAOImpl;
import fr.esiee.dao.VoyageurDAOImpl;
import fr.esiee.model.AdressePostale;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;
import java.util.List;  // Import de l'interface List
import fr.esiee.model.AdressePostale;
import fr.esiee.model.Bagage;
import fr.esiee.model.Voyageur;
import fr.esiee.dao.AdressePostaleDAOImpl;
import fr.esiee.dao.BagageDAOImpl;
import fr.esiee.dao.VoyageurDAOImpl;

public class Main2 {
    public static void main(String[] args) {
        // Création des adresses postales
        AdressePostale adresse1 = new AdressePostale(1, "123", "Rue A", "Ville A", "75001", "Pays A");
        AdressePostale adresse2 = new AdressePostale(2, "456", "Rue B", "Ville B", "75002", "Pays B");

        // Création des bagages (remplacez par les paramètres réels)
        Bagage bagage1 = new Bagage("40", "Rouge", 23.5);
        Bagage bagage2 = new Bagage("50", "Bleu", 20.0);

        // Création des voyageurs
        Voyageur voyageur1 = new Voyageur("Alice", 30, adresse1, bagage1);
        Voyageur voyageur2 = new Voyageur("Bob", 25, adresse2, bagage2);

        // Création des DAO et insertion des données
        AdressePostaleDAOImpl adressePostaleDAO = new AdressePostaleDAOImpl();
        BagageDAOImpl bagageDAO = new BagageDAOImpl();
        VoyageurDAOImpl voyageurDAO = new VoyageurDAOImpl();

        adressePostaleDAO.addAdressePostale(adresse1);
        adressePostaleDAO.addAdressePostale(adresse2);

        bagageDAO.addBagage(bagage1);
        bagageDAO.addBagage(bagage2);

        voyageurDAO.addVoyageur(voyageur1);
        voyageurDAO.addVoyageur(voyageur2);


        System.out.println("Insertion réalisée avec succès !");
    }
}


