package fr.esiee.model;

public class Bagage {
    private String numero;
    private String couleur;
    private double poids;

    public Bagage(String numero, String couleur, double poids) {
        this.numero = numero;
        this.couleur = couleur;
        this.poids = poids;
    }

    public void afficher() {
        System.out.println("Bagage - Numéro: " + numero + ", Couleur: " + couleur + ", Poids: " + poids + " kg");
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public double getPoids() {
        return poids;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }
}
