package fr.esiee.model;

public class AdressePostale {
    private int id;
    private String numeroVoie;
    private String rue;
    private String ville;
    private String codePostal;
    private String pays;

    public AdressePostale() {
    }

    public AdressePostale(int id, String numeroVoie, String rue, String ville, String codePostal, String pays) {
        this.id = id;
        this.numeroVoie = numeroVoie;
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
        this.pays = pays;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroVoie() {
        return numeroVoie;
    }

    public void setNumeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    // Méthode toString pour l'affichage des données
    @Override
    public String toString() {
        return "AdressePostale{" +
                "id=" + id +
                ", numeroVoie='" + numeroVoie + '\'' +
                ", rue='" + rue + '\'' +
                ", ville='" + ville + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", pays='" + pays + '\'' +
                '}';
    }
}
