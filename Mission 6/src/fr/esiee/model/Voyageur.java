package fr.esiee.model;

public class Voyageur {
    private String nom;
    private int age;
    private String categorie;
    private AdressePostale adresse;
    private Bagage bagage;
    public Voyageur(String nom, int age, AdressePostale adresse, Bagage bagage) {
        setNom(nom);
        setAge(age);
        this.adresse = adresse;
        setBagage(bagage);
    }
    public void afficherInfo() {
        System.out.println("Nom: " + nom + ", Age: " + age+ ", Adresse: " + adresse.getNumeroVoie() + " " +
                adresse.getVille() + " " + adresse.getCodePostal() +
                ", Bagage: " + bagage.getNumero() + " " + bagage.getCouleur() + " " + bagage.getPoids() + " kg");
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;
        } else {
            System.out.println("Le nom doit comporter au moins 2 caractères.");
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
            if (age < 1) {
                categorie = "nourrisson";
            } else if (age <= 18) {
                categorie = "enfant";
            } else if (age <= 60) {
                categorie = "adulte";
            } else {
                categorie = "senior";
            }
        } else {
            System.out.println("L'âge doit être positif.");
        }

    }
    public AdressePostale getAdresse() {
        return adresse;
    }

    public void setAdresse(AdressePostale adresse) {
        this.adresse = adresse;
    }
    public Bagage getBagage() {
        return bagage;
    }

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

}

