package fr.esiee;

import fr.esiee.model.Voyageur;
import fr.esiee.dao.VoyageurDAOImpl;
import java.util.List;

public class Main4 {
    public static void main(String[] args) {
        VoyageurDAOImpl voyageurDAO = new VoyageurDAOImpl();

        List<Voyageur> voyageurs = voyageurDAO.getAllVoyageurs();

        for (Voyageur voyageur : voyageurs) {
            // Afficher les informations du voyageur
            voyageur.afficherInfo();
        }
    }
}
