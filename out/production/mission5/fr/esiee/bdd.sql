-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 23 nov. 2023 à 14:06
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `easyline`
--

-- --------------------------------------------------------

--
-- Structure de la table `adressepostale`
--

CREATE TABLE `adressepostale` (
  `id` int(11) NOT NULL,
  `numero_libelle` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `code_postal` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `adressepostale`
--

INSERT INTO `adressepostale` (`id`, `numero_libelle`, `ville`, `code_postal`) VALUES
(1, '123 Rue de la Paix', 'Paris', '75001'),
(2, '456 Avenue des Champs-Élysées', 'Paris', '75008'),
(3, '789 Rue du Faubourg Saint-Honoré', 'Paris', '75008');

-- --------------------------------------------------------

--
-- Structure de la table `bagage`
--

CREATE TABLE `bagage` (
  `id` int(11) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `couleur` varchar(255) DEFAULT NULL,
  `poids` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `bagage`
--

INSERT INTO `bagage` (`id`, `numero`, `couleur`, `poids`) VALUES
(1, 1, 'Noir', 10.5),
(2, 2, 'Rouge', 8),
(3, 3, 'Bleu', 12.3);

-- --------------------------------------------------------

--
-- Structure de la table `voyage`
--

CREATE TABLE `voyage` (
  `id` int(11) NOT NULL,
  `voyageur_id` int(11) DEFAULT NULL,
  `bagage_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `voyage`
--

INSERT INTO `voyage` (`id`, `voyageur_id`, `bagage_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Structure de la table `voyageur`
--

CREATE TABLE `voyageur` (
  `id` int(11) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `adresse_postale_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `voyageur`
--

INSERT INTO `voyageur` (`id`, `prenom`, `nom`, `age`, `adresse_postale_id`) VALUES
(1, 'Nael', 'Mokrane', 25, 1),
(2, 'Dwayne', 'Johnson', 35, 2),
(3, 'Abdelali', 'jspcquoisonnomdefamille', 45, 3);

-- --------------------------------------------------------

--
-- Structure de la table `voyageurparticularite`
--

CREATE TABLE `voyageurparticularite` (
  `id` int(11) NOT NULL,
  `particularite` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `voyageurparticularite`
--

INSERT INTO `voyageurparticularite` (`id`, `particularite`) VALUES
(3, 'VIP');

-- --------------------------------------------------------

--
-- Structure de la table `voyageurprivilege`
--

CREATE TABLE `voyageurprivilege` (
  `id` int(11) NOT NULL,
  `code_privilege` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `voyageurprivilege`
--

INSERT INTO `voyageurprivilege` (`id`, `code_privilege`) VALUES
(1, 'GOLD'),
(2, 'SILVER');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adressepostale`
--
ALTER TABLE `adressepostale`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bagage`
--
ALTER TABLE `bagage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `voyage`
--
ALTER TABLE `voyage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voyageur_id` (`voyageur_id`),
  ADD KEY `bagage_id` (`bagage_id`);

--
-- Index pour la table `voyageur`
--
ALTER TABLE `voyageur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adresse_postale_id` (`adresse_postale_id`);

--
-- Index pour la table `voyageurparticularite`
--
ALTER TABLE `voyageurparticularite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `voyageurprivilege`
--
ALTER TABLE `voyageurprivilege`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adressepostale`
--
ALTER TABLE `adressepostale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `bagage`
--
ALTER TABLE `bagage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `voyage`
--
ALTER TABLE `voyage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `voyageur`
--
ALTER TABLE `voyageur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `voyage`
--
ALTER TABLE `voyage`
  ADD CONSTRAINT `voyage_ibfk_1` FOREIGN KEY (`voyageur_id`) REFERENCES `voyageur` (`id`),
  ADD CONSTRAINT `voyage_ibfk_2` FOREIGN KEY (`bagage_id`) REFERENCES `bagage` (`id`);

--
-- Contraintes pour la table `voyageur`
--
ALTER TABLE `voyageur`
  ADD CONSTRAINT `voyageur_ibfk_1` FOREIGN KEY (`adresse_postale_id`) REFERENCES `adressepostale` (`id`);

--
-- Contraintes pour la table `voyageurparticularite`
--
ALTER TABLE `voyageurparticularite`
  ADD CONSTRAINT `voyageurparticularite_ibfk_1` FOREIGN KEY (`id`) REFERENCES `voyageur` (`id`);

--
-- Contraintes pour la table `voyageurprivilege`
--
ALTER TABLE `voyageurprivilege`
  ADD CONSTRAINT `voyageurprivilege_ibfk_1` FOREIGN KEY (`id`) REFERENCES `voyageur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
