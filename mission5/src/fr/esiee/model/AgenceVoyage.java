package fr.esiee.model;

import java.util.ArrayList;

public class AgenceVoyage {
    private String nom;
    private String adresse;
    private ArrayList<Voyageur> listeVoyageurs;

    public AgenceVoyage(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
        this.listeVoyageurs = new ArrayList<>();
    }

    public void ajouterVoyageur(Voyageur voyageur) {
        this.listeVoyageurs.add(voyageur);
    }

    public void afficherInformations() {
        System.out.println("Nom de l'agence: " + nom);
        System.out.println("Adresse de l'agence: " + adresse);
        System.out.println("Liste des voyageurs:");

        for (Voyageur voyageur : listeVoyageurs) {
            voyageur.afficher();
        }
    }

    // Getters and setters
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}

