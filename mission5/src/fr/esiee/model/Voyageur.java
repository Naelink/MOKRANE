package fr.esiee.model;

public class Voyageur {
    private final String prenom;
    private final int id;
    protected String nom;
    protected int age;

    public Voyageur(int id, String nom, String prenom, int age) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }


    public void afficher() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Nom: " + nom + ", Âge: " + age;
    }

    // Getters and setters
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getId() {
        return id;
    }

    public String getPrenom() {
        return prenom;
    }
}
