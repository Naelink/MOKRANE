package fr.esiee.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class demande1 {
    public static List<Voyageur> getVoyageursParLettre(String lettre) {
        String url = "jdbc:mysql://localhost:3306/easyline";
        String user = "nael";
        String pwd = "toor";
        try {
            Connection conn=DriverManager.getConnection(url,user,pwd);
            System.out.println("Connexion OK");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Voyageur> voyageurs = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, user, pwd)) {
            // Création de la requête SQL
            String query = "SELECT * FROM Voyageur WHERE nom LIKE ? OR prenom LIKE ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, lettre + "%");
                preparedStatement.setString(2, lettre + "%");

                // Exécution de la requête
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        // Récupération des données
                        int id = resultSet.getInt("id");
                        String nom = resultSet.getString("nom");
                        String prenom = resultSet.getString("prenom");
                        int age = resultSet.getInt("age");

                        Voyageur voyageur = new Voyageur(id, nom, prenom, age);
                        voyageurs.add(voyageur);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return voyageurs;
    }
    public static void main(String[] args) {
        List<Voyageur> voyageursAvecLettre = getVoyageursParLettre("M");

        for (Voyageur voyageur : voyageursAvecLettre) {
            System.out.println("ID: " + voyageur.getId() + ", Nom: " + voyageur.getNom() +
                    ", Prénom: " + voyageur.getPrenom() + ", Age: " + voyageur.getAge());
        }
    }
}