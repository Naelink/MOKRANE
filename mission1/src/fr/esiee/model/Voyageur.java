package fr.esiee.model;

public class Voyageur {
    private String nom;
    private int age;
    private String categorie;
    public Voyageur() {
    }
    public Voyageur(String nom, int age) {
        setNom(nom);
        setAge(age);
    }
    public void afficherInfo() {
        System.out.println("Nom: " + nom + ", Age: " + age+ ", Catégorie: " + categorie);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        if (nom.length() >= 2) {
            this.nom = nom;
        } else {
            System.out.println("Le nom doit comporter au moins 2 caractères.");
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
            if (age < 1) {
                categorie = "nourrisson";
            } else if (age <= 18) {
                categorie = "enfant";
            } else if (age <= 60) {
                categorie = "adulte";
            } else {
                categorie = "senior";
            }
        } else {
            System.out.println("L'âge doit être positif.");
        }
    }
}

