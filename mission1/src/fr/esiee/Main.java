package fr.esiee;
import java.util.*;
import fr.esiee.model.Voyageur;

public class Main {
    public static void main(String[] args) {
        // a.
        Voyageur voyageur2arguments = new Voyageur("Nael Mokrane", 20);
        voyageur2arguments.afficherInfo();

        // b.
        Voyageur voyageurpardefaut = new Voyageur();
        // c.
        Scanner sc = new Scanner(System.in); //System.in is a standard input stream
        System.out.print("Entrez le nom du deuxième voyageur: ");
        voyageurpardefaut.setNom(sc.nextLine());
        System.out.print("Entrez l'âge du deuxième voyageur: ");
        voyageurpardefaut.setAge(Integer.parseInt(sc.next()));
        voyageurpardefaut.afficherInfo();

    }
}
