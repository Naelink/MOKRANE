package fr.esiee.model;

import java.util.ArrayList;

public class AgenceVoyage {
    private String nom;
    private String adresse;
    private ArrayList<Voyageur> listeVoyageurs;

    public AgenceVoyage(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
        this.listeVoyageurs = new ArrayList<>();
        // Initialiser avec au moins cinq voyageurs
        this.listeVoyageurs.add(new Voyageur("Alice", 30));
        this.listeVoyageurs.add(new Voyageur("Bob", 25));
        this.listeVoyageurs.add(new Voyageur("Charlie", 40));
        this.listeVoyageurs.add(new Voyageur("David", 28));
        this.listeVoyageurs.add(new Voyageur("Eve", 35));
    }

    public void afficherInformations() {
        System.out.println("Nom de l'agence : " + nom);
        System.out.println("Adresse de l'agence : " + adresse);
        System.out.println("Liste des voyageurs :");
        for (Voyageur voyageur : listeVoyageurs) {
            voyageur.afficherInfo();
        }
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void ajouterVoyageur(Voyageur voyageur) {
        this.listeVoyageurs.add(voyageur);
    }

    public Voyageur recupererVoyageurParNom(String nom) {
        for (Voyageur voyageur : listeVoyageurs) {
            if (voyageur.getNom().equalsIgnoreCase(nom)) {
                return voyageur;
            }
        }
        return null;
    }

    public void supprimerVoyageurParNom(String nom) {
        for (int i = 0; i < listeVoyageurs.size(); i++) {
            if (listeVoyageurs.get(i).getNom().equalsIgnoreCase(nom)) {
                listeVoyageurs.remove(i);
                break;
            }
        }
    }
}
