package fr.esiee;

import fr.esiee.model.Voyageur;
import fr.esiee.model.AgenceVoyage;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // a.
        AgenceVoyage agence = new AgenceVoyage("Skypiea Voyages", "123 Rue de la Ville, Paris");

        // b.
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nChoisissez une option :");
            System.out.println("1. Ajout d'un nouveau voyageur");
            System.out.println("2. Recherche d'un utilisateur par son nom et son affichage");
            System.out.println("3. Suppression d'un utilisateur par son nom et son affichage avant de supprimer pour confirmer");
            System.out.println("4. Affichage des informations de l'agence de voyage (nom, adresse et liste des voyageurs)");
            System.out.println("5. Quitter l'application");

            int choix = scanner.nextInt();
            scanner.nextLine(); // pour consommer la nouvelle ligne

            switch (choix) {
                case 1:
                    System.out.print("Nom du nouveau voyageur : ");
                    String nom = scanner.nextLine();
                    System.out.print("Âge du nouveau voyageur : ");
                    int age = scanner.nextInt();
                    scanner.nextLine(); // pour consommer la nouvelle ligne
                    Voyageur nouveauVoyageur = new Voyageur(nom, age);
                    agence.ajouterVoyageur(nouveauVoyageur);
                    break;

                case 2:
                    System.out.print("Entrez le nom du voyageur : ");
                    String nomRecherche = scanner.nextLine();
                    Voyageur voyageurRecherche = agence.recupererVoyageurParNom(nomRecherche);
                    if (voyageurRecherche != null) {
                        voyageurRecherche.afficherInfo();
                    } else {
                        System.out.println("Aucun voyageur trouvé avec ce nom.");
                    }
                    break;

                case 3:
                    System.out.print("Entrez le nom du voyageur à supprimer : ");
                    String nomSuppression = scanner.nextLine();
                    Voyageur voyageurSuppression = agence.recupererVoyageurParNom(nomSuppression);
                    if (voyageurSuppression != null) {
                        voyageurSuppression.afficherInfo();
                        System.out.print("Confirmez-vous la suppression ? (O/N) : ");
                        String confirmation = scanner.nextLine();
                        if (confirmation.equalsIgnoreCase("O")) {
                            agence.supprimerVoyageurParNom(nomSuppression);
                            System.out.println("Voyageur supprimé avec succès.");
                        } else {
                            System.out.println("Suppression annulée.");
                        }
                    } else {
                        System.out.println("Aucun voyageur trouvé avec ce nom.");
                    }
                    break;

                case 4:
                    agence.afficherInformations();
                    break;

                case 5:
                    System.out.println("Merci d'avoir utilisé notre application. Au revoir !");
                    scanner.close();
                    System.exit(0);

                default:
                    System.out.println("Option non valide. Veuillez choisir une option valide.");
            }
        }
    }
}

