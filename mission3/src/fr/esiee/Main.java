package fr.esiee;
import fr.esiee.model.AdressePostale;
import fr.esiee.model.Voyageur;
import fr.esiee.model.Bagage;

public class Main {
    public static void main(String[] args) {

        // a.
        AdressePostale adresse = new AdressePostale("123 Rue de la Ville", "Paris", "75001");
        Bagage bagage = new Bagage("B123", "Noir", 12.5);
        Voyageur voyageur = new Voyageur("John Doe", 25, adresse, bagage);
        voyageur.afficherInfo(); // Affiche : Nom: John Doe, Age: 25, Adresse: 123 Rue de la Ville Paris 75001, Bagage: B123 Noir 12.5 kg

        // b.
        Bagage nouveauBagage = new Bagage("B456", "Bleu", 15.0);
        voyageur.setBagage(nouveauBagage);
        voyageur.afficherInfo(); // Affiche : Nom: John Doe, Age: 25, Adresse: 123 Rue de la Ville Paris 75001, Bagage: B456 Bleu 15.0 kg

        // c.
        voyageur.getBagage().setCouleur("Rouge");
        voyageur.afficherInfo(); // Affiche : Nom: John Doe, Age: 25, Adresse: 123 Rue de la Ville Paris 75001, Bagage: B456 Rouge 15.0 kg

        // d.
        voyageur.setBagage(null);
        voyageur.afficherInfo(); // Affiche : Nom: John Doe, Age: 25, Adresse: 123 Rue de la Ville Paris 75001, Bagage: null
    }
}

