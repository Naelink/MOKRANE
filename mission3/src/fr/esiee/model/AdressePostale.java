package fr.esiee.model;

public class AdressePostale {
    private String numeroVoie;
    private String ville;
    private String codePostal;

    public AdressePostale(String numeroVoie, String ville, String codePostal) {
        this.numeroVoie = numeroVoie;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public void afficher() {
        System.out.println(numeroVoie + " " + ville + " " + codePostal);
    }

    public String getNumeroVoie() {
        return numeroVoie;
    }

    public void setNumeroVoie(String numeroVoie) {
        this.numeroVoie = numeroVoie;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }
}

